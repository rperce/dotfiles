if status --is-interactive
    set -g fish_user_abbreviations
    abbr -a vim nvim
    abbr -a gs 'git status'
    abbr -a gh 'git log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short'
    abbr -a rm 'rm -i'
    abbr -a '&&' '; and'
end

alias la 'ls -a'
alias ll 'ls -alh'
alias lx 'ls -lXB'

alias q exit
alias ixio "curl -F 'f:1=<-' ix.io"

alias s substitute

set fish_greeting ''

set -gx VISUAL nvim
set -gx EDITOR nvim
set -gx PATH /home/robertp/path/ $PATH

status --is-interactive; and source (rbenv init -|psub)

set -gx GO111MODULE 'on'
set -gx GOPROXY 'https://modprox-proxy.corp.indeed.com/'
set -gx TAGGIT_REGISTRY_URL 'https://mods.sandbox.indeed.net/'
set -gx GOROOT '/usr/local/go'

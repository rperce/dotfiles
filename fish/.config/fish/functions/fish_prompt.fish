set fish_prompt_pwd_dir_length 0
function fish_prompt
    set line green

    set_color -o $line
    echo -n '╔═('
    set_color -o blue
    echo -n (whoami)'@'(hostname)
    set_color -o $line
    echo -n ')════('
    set_color -o cyan
    echo -n (prompt_pwd)
    set_color -o $line
    echo ')'
    echo -n '╚══~> '
    set_color normal
end

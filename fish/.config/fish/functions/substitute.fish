function substitute
    set -l last $history[1]
    switch (count $argv)
        case 3
            if test $argv[3] = 'g'
                commandline (echo $last | sed "s/$argv[1]/$argv[2]/g")
                commandline -f execute
            else
                echo "Syntax: substitute <find> <replace> [g]"
            end
        case 2
            commandline (echo $last | sed "s/$argv[1]/$argv[2]/")
            commandline -f execute
        case '*'
            echo "Syntax: substitute <find> <replace> [g]"
    end
end

-- Standard awesome library
local awful = require("awful")
require("awful.autofocus")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

awful.spawn.with_shell("$HOME/path/setpath")
awful.spawn.with_shell("setxkbmap -option compose:ralt -option caps:ctrl_modifier -option terminate:ctrl_alt_bksp")

awful.spawn.with_shell("picom -bc")
awful.spawn.with_shell("unclutter")

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(awful.util.getdir("config") .. "/themes/tron/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "termite -e 'tmux -2'"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

    -- {{{ Wibar
require("config/wibar")
-- }}}

-- {{{ Bindings
require("config/bindings")
-- }}}

-- {{{ Rules
require("config/rules")
-- }}}

-- {{{ Signals
require("config/signals")
-- }}}

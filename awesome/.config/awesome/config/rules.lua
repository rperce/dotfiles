local awful = require("awful")
local beautiful = require("beautiful")

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "pinentry",
        },
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Don't add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" } },
        properties = { titlebars_enabled = false }
    },

    -- Transparency
    { rule_any = { class = { "Termite" } },
        properties = { opacity = 0.8 }
    },

    -- Steam on "game"
    { rule = { class = "Steam" },
        properties = { screen = 1, tag = "game" }
    },
}
-- }}}



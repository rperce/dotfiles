local awful     = require('awful')
local naughty   = require('naughty')
local vicious   = require('vicious')
local wibox     = require('wibox')
local margins = {
    right = 5,
    left = 0,
    top = 3,
    bottom = 3
}
local alsawidget = {
    step = '5',
    control = 'pamixer',
    colors = {
        unmute = '#AECF96',
        mute = '#FF5656'
    },
    mixer = 'pavucontrol', -- or whatever your preferred sound mixer is
    notifications = {
        icons = {},
        font = 'Monospace 11', -- must be a monospace font for the bar to be sized consistently
        icon_size = 48,
        bar_size = 20 -- adjust to fit your font if the bar doesn't fit
    },
}
alsawidget.amixer_cmd = function(op, cmd)
	if cmd then
		op = op .. ' ' .. cmd
	end
    return alsawidget.control .. ' ' .. op
end
alsawidget.fn_spawn_mixer = function()
    awful.util.spawn(alsawidget.mixer)
end
alsawidget.fn_toggle_mute = function()
    awful.util.spawn(alsawidget.amixer_cmd('-t'))
    vicious.force ({ alsawidget.bar })
end
alsawidget.fn_vol_up = function()
    awful.util.spawn(alsawidget.amixer_cmd('-i', alsawidget.step))
    vicious.force ({ alsawidget.bar })
end
alsawidget.fn_vol_down = function()
    awful.util.spawn (alsawidget.amixer_cmd('-d', alsawidget.step))
    vicious.force ({ alsawidget.bar })
end

-- widget
alsawidget.bar = wibox.widget{
    background_color    = '#494b4f',
    widget              = wibox.widget.progressbar,
    color               = alsawidget.colors.unmute,
}
alsawidget.widget = wibox.container.margin(wibox.widget{
    alsawidget.bar,
    forced_height       = 24,
    forced_width        = 24,
    direction           = 'east',
    layout              = wibox.container.rotate
}, 5, 0, 1, 1)
--alsawidget.bar:set_background_color ('#494B4F')
alsawidget.widget:buttons (awful.util.table.join (
    awful.button ({}, 1, alsawidget.fn_spawn_mixer),
    awful.button ({}, 3, alsawidget.fn_toggle_mute),
    awful.button ({}, 4, alsawidget.fn_vol_up),
    awful.button ({}, 5, alsawidget.fn_vol_down)
))

-- tooltip
alsawidget.tooltip = awful.tooltip ({ objects = { alsawidget.widget } })
-- naughty notifications
alsawidget._current_level = 0
alsawidget._muted = false
--
-- register the widget through vicious
vicious.register (alsawidget.bar, vicious.widgets.volume, function (bar, args)
    local f = io.popen("pamixer --get-volume")
    local v = f:read("*all")
    f:close()
    -- naughty.notify { text = v }
    alsawidget._current_level = v
    bar:set_value(v)
    if args[2] == '♩' then
        alsawidget._muted = true
        alsawidget.tooltip:set_text (' [Muted] ')
        bar.color = alsawidget.colors.mute
        return 100
    end
    alsawidget._muted = false
    alsawidget.tooltip:set_text (' ' .. v .. '% ')
    bar.color = alsawidget.colors.unmute
    --widget:set_color(alsawidget.colors.unmute)
    return v
end, 5, alsawidget.channel) -- relatively high update time, use of keys/mouse will force update

return alsawidget

local wibox     = require('wibox')
local gears     = require('gears')
local awful     = require('awful')

read = awful.spawn.easy_async_with_shell

timers = {
    get = function(index)
        if timers[index] ~= nil then
            return timers[index]
        else
            timers[index] = gears.timer{ timeout = index }
            return timers[index]
        end
    end
}

hook_timer_with_cmd = function(timeout, textbox, cmd, fn)
    timer = timers.get(timeout)
    if cmd ~= nil then
        timer:connect_signal('timeout', function()
            read(cmd, function(text)
                if text == nil then
                    text = 'nil'
                end
                textbox.markup = '<span>' .. text .. '</span>'
            end)
        end)
        timer:emit_signal('timeout')
        if not(timer.started) then
            timer:start()
        end
    end
end

out = {}
out.new = function(table)
    table.timeout = table.timeout or 10
    table.tooltip_timeout = table.tooltip_timeout or table.timeout

    local widget = wibox.widget.textbox(table.default)
    hook_timer_with_cmd(table.timeout, widget, table.cmd, table.fn)
    if table.tooltip_default ~= nil then
        tooltip = awful.tooltip({ objects = { widget }})
        tooltip:set_text(table.tooltip_default)
        hook_timer_with_cmd(table.tooltip_timeout, tooltip, table.tooltip_cmd)
    end
    return widget
end

return out

local timedcmd = require('widgets/timedcmd')

return timedcmd.new({
    default           = 'Time',
    cmd               = '$HOME/path/wordtime -t',
    tooltip_default   = 'Date/Time',
    tooltip_cmd       = '$HOME/path/wordtime'
})

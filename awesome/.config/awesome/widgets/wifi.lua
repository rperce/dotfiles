local timedcmd = require('widgets/timedcmd')

local cmd = "$HOME/path/wifi_status"
return timedcmd.new({
    default = 'Network',
    cmd     = '$HOME/path/wifi_status',
})

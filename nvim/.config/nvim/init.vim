scriptencoding utf-8
call plug#begin()
Plug 'vim-scripts/vimwiki'
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
Plug 'tommcdo/vim-lion'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-commentary'
Plug 'wellle/targets.vim'
Plug 'w0rp/ale'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'

Plug 'modille/groovy.vim', { 'for': 'groovy' }
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascript.jsx', 'typescript', 'typescript.tsx'] }
Plug 'mxw/vim-jsx', { 'for': ['javascript', 'javascript.jsx', 'typescript', 'typescript.tsx'] }
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
Plug 'rperce/vim-homebrewery', { 'for': 'homebrew' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'leafgarland/typescript-vim', { 'for': ['typescript', 'typescript.tsx'] }
Plug 'peitalin/vim-jsx-typescript', { 'for': 'typescript.tsx' }
Plug 'kchmck/vim-coffee-script', { 'for': 'coffee' }
Plug 'dag/vim-fish', { 'for': 'fish' }
Plug 'fatih/vim-go', { 'for': 'go' }
Plug 'vim-crystal/vim-crystal'
call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""
" General
"""""""""""""""""""""""""""""""""""""""""""""""""""
" How much history do we care about (in terms of lines of commands)?
set history=700

" Auto-update when a file is externally changed
set autoread

" Extra key combos!
let g:mapleader = "\<Space>"

" Faster saving
noremap <leader>w :w<cr>

" h and l on the edges of lines wrap around
set whichwrap+=<,>,h,l

" When I join lines, don't put two spaces there if there's a sentence break
set nojoinspaces

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Visual differences
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Line numbers, PLEASE
set relativenumber
set number

" I'll probably never be in a non-dark terminal
set background=dark

" Colored column at column 90
set colorcolumn=90

" Don't highlight my searches after I'm done searching
set nohlsearch

" Current cursor's line underlined
set cursorline
hi clear CursorLine
hi CursorLine gui=underline cterm=underline

" colorscheme
colorscheme muon
" muon has really dark, unreadable comments. Let's brighten them up!
hi Comment ctermfg=DarkYellow cterm=bold

" Literal tabs and trailing whitespace visible
set listchars=tab:┆-,trail:~,extends:>,precedes:<,nbsp:•
set list
hi Whitespace ctermfg=DarkGray cterm=bold
hi NonText ctermfg=DarkGray

" I prefer the way lightline displays it
set noshowmode

" Have at least three lines at the edge of the screen
" for some context, as long as we scrolled there
set scrolloff=3

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Behavior
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Be sensible about cases when searching
set ignorecase
set smartcase

" and move as search is typed
set incsearch

" Allow me to click if I'm in an X server
set mouse=a

" All my code is in git anyways
set nobackup
set nowritebackup
set noswapfile

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Indents and lines
"""""""""""""""""""""""""""""""""""""""""""""""""""
" don't softwrap lines, but autoindent them
set nowrap
set autoindent

" All I want for Christmas is spaces
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4

" Hard wrap text at 90 characters (80 plus some wiggle room)
set textwidth=90

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Extra commands
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Fumble-finger saving
command! W w
command! Wq wq
command! WQ wq
command! Q q
command! Wa wa
command! WA wa

" Who wants to move their pinkies? (jk exists in _no_ english words)
inoremap jk <Esc>

" Treat long lines as broken lines so you can move in them
nnoremap j gj
nnoremap k gk

" s and S work like i and a, but for a single character at a time (like r)
nnoremap s :<C-U>exec "normal i" . InitMethods#RepeatInput()<CR>
nnoremap S :<C-U>exec "normal a" . InitMethods#RepeatInput()<CR>

" Move between splits a little easier
noremap <C-j> <C-W>j
noremap <C-k> <C-W>k
noremap <C-h> <C-W>h
noremap <C-l> <C-W>l
noremap <Tab> <C-W>w
noremap <S-Tab> <C-W>W

" fast split opening
nnoremap <expr><silent> \| !v:count ? "<C-W>v<C-W><Right>" : '\|'
nnoremap <expr><silent> _  !v:count ? "<C-W>s<C-W><Down>"  : '_'
set splitbelow
set splitright

" Tab management
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tx :tabclose<cr>

" Open a new tab with current buffer's path
nnoremap <leader>te :tabedit <c-r>=expand("%:p:h")<cr>/<cr>

" Kill the Windows ^M if the encodings are screwy
nnoremap <leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Edit and source config
nnoremap <leader>ev :vs $MYVIMRC<CR>
nnoremap <leader>sv :so %<CR>

" murder trailing whitespace
nnoremap <leader>kw :call InitMethods#DeleteTrailingWS()<cr>

" execute visually-selected text as command
vnoremap <space> "xy:@x<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin Config
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Set vimwiki to be its own dir and use markdown syntax
let g:vimwiki_list = [{'path': '~/vimwiki',
                     \ 'syntax': 'markdown',
                     \ 'ext': '.md'}]

" But don't mess with markdown files outside of the wiki dir
let g:vimwiki_global_ext = 0

" cutesy separators in lightline
let g:lightline = {
    \ 'separator': {'left': '▓▓▒', 'right': '▒▓▓'},
    \ 'subseparator': {'left': '⡇⡷', 'right': '⢾⢸'}
    \ }

" read jsx syntax in .js files too
let g:jsx_ext_required = 0

" tell neovim where python is
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

let g:ale_fix_on_save = 1
let g:ale_fixers = {
    \ 'json': ['prettier'],
    \ 'javascript': ['eslint', 'prettier'],
    \ 'javascript.jsx': ['prettier'],
    \ 'typescript': ['prettier'],
    \ 'typescript.tsx': ['prettier'],
    \ 'ruby': ['rubocop']
    \ }
"""""""""""""""""""""""""""""""""""""""""""""""""""
" Miscellaneous
"""""""""""""""""""""""""""""""""""""""""""""""""""

" get highlight group of word under cursor
nnoremap <leader>hi :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" even faster fuzzy finding
nnoremap <leader>f :FZF<CR>

nnoremap <silent> <F1> :UndotreeToggle<CR>

nnoremap <leader>kw :call InitMethods#DeleteTrailingWS()<cr>

let g:jsx_ext_required = 0

nnoremap <leader>b :Buffers<CR>

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

let g:typescript_indent_required = 0
let g:go_version_warning = 0
let g:go_fmt_command = "goimports"

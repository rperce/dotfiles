#
# ~/.bashrc
#
DOT_PATH_FILE='$HOME/path/setpath'
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# don't put duplicate lines in history
HISTCONTROL=$HISTCONTROL${HISTCONTROL+..}ignoredups
shopt -s histappend

# Two-line Prompt
line='\[\033[1;32m\]' #green
blue='\[\033[1;34m\]'
dblue='\[\033[0;34m\]'
white='\[\033[0;37m\]'
cyan='\[\033[1;36m\]'
dred='\[\033[0;31m\]'
pink='\[\033[1;35m\]'
dgreen='\[\033[0;32m\]'
reset='\[\033[00m\]'
prompt_color() {
    status=$(git status)
    if (ls `git rev-parse --git-dir` | grep -q rebase); then #rebase
        printf $pink
        return
    elif [ -f $(git rev-parse --git-dir)/MERGE_HEAD ]; then #merge
        printf $pink
        return
    fi

    st=$(git status --porcelain)
    if (echo "$st" | grep -q "^ M"); then #unstaged changes
        printf $dred
        return
    elif (echo "$st" | grep -q "^??"); then #untracked files
        printf $dgreen
        return
    fi
    printf $line
}
prompt_char() {
    if (which git >/dev/null) && (git rev-parse --is-inside-work-tree 2>/dev/null >/dev/null); then
        printf "$(prompt_color)λ"
        return
    fi
    printf '$'
}

_ps1() {
    local user_host=$blue'\u@\h'$line
    local history=$white'\!'$line
    local cwd=$cyan'\w'$line
    export PS1=$line'╔═('$user_host')════['$history']════('$cwd')\n'$line'╚══╡'$(prompt_char)$reset' '
}

PROMPT_COMMAND=_ps1
PS2="$line╚╡$reset "

# useful ls
alias ls='ls -F --color=auto'
alias la='ls -a'
alias ll='ls -alh'
alias lx='ls -lXB'

# safety
alias rm='rm -i'

# include .bash_aliases
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f $DOT_PATH_FILE ]; then
    . $DOT_PATH_FILE
fi
# shh, termite. The world is not ready for you.
if [ "$TERM" = "xterm-termite" ]; then
    export TERM='xterm'
fi

# set colors if in a framebuffer terminal to those of .Xresources
if [ "$TERM" = "linux" ]; then
    colors=(\#222222 \#9e5641 \#6c9e55 \#caaf2b \#7fb8d8 \#956d9d \#4c8ea1 \#808080
            \#454545 \#cc896d \#64e070 \#ffe080 \#b8ddea \#c18fcb \#44c0ff \#cdcdcd)
    for n in $(seq 0 15); do
        printf '\e]P%x%s' $n $(echo "${colors[$n]}" | tr -d \#)
    done
    clear; unset colors
fi

bind 'set mark-symlinked-directories on'
export EDITOR='/usr/bin/nvim'
export VISUAL='/usr/bin/nvim'
export JAVA_HOME='/usr/lib/jvm/java-8-openjdk/'

# git branch tab completion
test -f ~/.git-completion.bash && . $_
test -f /usr/share/fzf/key-bindings.bash && . $_
test -f /usr/share/fzf/completion.bash && . $_

export HISTCONTROL='ignoreboth'
export HISTIGNORE='ls:cd:q'

[ -n "$XTERM_VERSION" ] && transset-df -a > /dev/null

export FZF_DEFAULT_COMMAND='rg --files'

export RUST_SRC_PATH="$HOME/.multirust/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"

GPG_TTY=$(tty); export GPG_TTY;
PATH="$HOME/perl5/bin${PATH+:}${PATH}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ -e "$HOME/env/etc/indeedrc" ]; then
    . "$HOME/env/etc/indeedrc"
fi

PATH=$HOME/.cargo/bin:$PATH

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

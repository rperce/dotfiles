#!/usr/bin/ruby
# frozen_string_literal: true

@small =  %w[zero one two three four five six seven eight nine ten eleven twelve] \
  + %w[thir four fif six seven eigh nine].map { |s| s + 'teen' }
@tens  =  ['', ''] + %w[twenty thirty forty fifty sixty seventy eighty ninety]
@scale =  [''] + %w[thousand million billion trillion quadrillion]

def tens(n)
    return @small[n] if n < 20

    q, r = n.divmod(10)
    out = @tens[q]
    out += "-#{@small[r]}" if r.positive?
    out
end

def hundreds(n)
    q, r = n.divmod(100)
    out = ''
    if q.positive?
        out = "#{@small[q]} hundred"
        out += ' and ' if r.positive?
    end
    out += tens(r) if r.positive?
    out
end

def thous(n, depth)
    q, r = n.divmod(1000)
    out = ''
    if q.positive?
        out = thous(q, depth + 1)
        out += ', ' if r.positive?
    end
    out += "#{hundreds(r)} #{@scale[depth]}" if r.positive?
    out
end

n = ARGV[0].to_i
if n.zero?
    puts 'zero'
else
    puts thous(n, 0)
end
